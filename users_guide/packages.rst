Packages
========

Third-party packages are kept inside of the ``packages`` folder. Because App Engine does not support packaging tools such as easy_install or pip, you will need to manually download and copy any external packages you wish to use inside of this directory. Ferris includes the ability to use packages that are in zip files within the package directory.


Included Third-Party Libraries
------------------------------

By default Ferris includes the following packages:

    * `pytz <https://code.google.com/p/gae-pytz/>`_
    * `Google API Client <https://code.google.com/p/google-api-python-client/>`_
    * `GData API Client <https://code.google.com/p/gdata-python-client/>`_
    * `WTForms <http://wtforms.readthedocs.org/en/latest/>`_
    * `Protopigeon <https://bitbucket.org/jonparrott/protopigeon>`_

