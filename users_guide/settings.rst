Settings
========

.. module:: ferris.core.settings

The settings module provides a way to specify application-wide settings in a centralized registry.

Configuration
-------------

To configure settings use ``settings.py`` in your application's root. There should already be some defaults configured but you may add your own::

    defaults['cats'] = {
        'herdable': False
    }

    settings.defaults(defaults)


To read your settings, import ``settings`` and use :func:`get`::

    from ferris import settings

    HERDABLE = settings.get('cats').get('herdable')

Functions
---------

.. autofunction:: defaults

.. autofunction:: settings

.. autofunction:: get


Plugin
------

.. module:: plugins.settings

The built-in settings plugin provides you with the ability to configure overrides to the default settings via a web interface.

To use the settings plugin make sure it's enabled in ``app/routes.py`` (preferably before any other plugins)::
    
    plugins.enable('settings')

You can then access the settings manager at `/admin/settings <http://localhost:8080/admin/settings>`_.

Any settings specified in the admin interface will take precendence over the ones specified in your ``settings.py`` file.

You can also add your own custom settings to the admin interface. To do so, subclass the setting model :class:`~plugins.settings.Setting`::

    from plugins.setting import Setting

    class CatsSetting(Setting):
        _setting_key = 'cats'
        _name = 'Cats'
        herdable = ndb.BooleanProperty(default=False)
        meows = ndb.IntegerProperty(default=5)

This is a normal :class:`model <ferris.core.ndb.Model>` class so regular ndb properties are used to specify fields. You must also specify the :attr:`~Setting._setting_key` and :attr:`~Setting._name` attributes.

.. class:: Setting

.. attribute:: Setting._setting_key

    The key where the settings will be stored. To get these settings use ``settings.get(_setting_key)``.

.. attribute:: Setting._name

    The name to display for this setting in the admin interface.
